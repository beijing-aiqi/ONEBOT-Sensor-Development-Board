/**************************************************
 文件：Application.C
 介绍：Main Application
**************************************************/
#include "common.h"

int main( void )
{
	/*S1：变量初始化 */
	uint8_t TH_Value[4];
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_THDisplay);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	AIQI_HTDisplay_Init();
	/*S5: 模块运行 */
	while ( 1 )
	{	
		/*Your code*/
		AIQI_HTDisplay_T_Display(4);//数码管显示温度 显示时间4s
		AIQI_HTDisplay_RH_Display(4);//数码管显示湿度 显示时间4s
		//S3 Send to CC
		TH_Value[0]=(uint8_t)AIQI_HTDisplay_Read_T();//读取温度 float型
		TH_Value[1]=(uint8_t)AIQI_HTDisplay_Read_RH();//读取湿度 float型
		AIQI_SendDataToCC(TH_Value);
		/*Your code end*/
	}
}
