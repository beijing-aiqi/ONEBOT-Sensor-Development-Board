/**************************************************
 文件：Application.C
 介绍：Main Application
**************************************************/
#include "common.h"


int main( void )
{
	/*S1：变量初始化 */
	float Temp=0,Humi=0;
	uint8_t TH_Value[4];
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_TH);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	AIQI_HT_Init();
	/*S5: 模块运行 */
	while ( 1 )
	{	
		/*Your code*/
		Humi=AIQI_Read_Humidity();//读取湿度 float型
		Temp=AIQI_Read_Temp();//读取温度 float型
		TH_Value[0]=Temp;
		TH_Value[1]=Humi;
		AIQI_SendDataToCC(TH_Value);//将数据发送向主控
		/*Your code end*/
	}
}
