/**************************************************
 文件：Application.C
 介绍：Main Application
**************************************************/
#include "common.h"

int main( void )
{
	/*S1：变量初始化 */
	//《生日歌》
	Music_Score MyMusic2[]=
	{  
	 {Sol_M,2},{Sol_M,1},{Sol_M,2},
	 {La_M,4},{Sol_M,4},{Do_H,4},
	 {Si_M,4},{Si_M,4},{Sol_M,2},{Sol_M,1},{Sol_M,1},
	 {La_M,4},{Sol_M,4},{Re_H,4},
	 {Do_H,4},{Do_H,4},{Sol_M,2},{Sol_M,1},{Sol_M,1},
	 {Sol_H,4},{Mi_H,4},{Do_H,4},
	 {Si_M,4},{La_M,4},{Fa_H,2},{Fa_H,1},{Fa_H,1},
	 {Mi_H,4},{Do_H,4},{Re_H,4},{Do_H,4},{Do_H,4},
	 {0,0}//必须00结束
	};
  uint8_t Beep_Value[4]={0};
	uint8_t Volume=0;
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_BEEP);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	//配置并开启PWM
	AIQI_IOConfig(IO6, IO_PWM, 2000, "pwm2"); // 频率=2000Hz，默认占空比=0%
	AIQI_PWM_Open("pwm2");					// PWM 开启
	/*S5: 模块运行 */
	while ( 1 )
	{	
			AIQI_Music_Tempo(180);//曲速 单位 拍/分钟
			if( AIQI_ReadDataFromCC(Beep_Value) != 0 )//从主控读取数据
			{
				Volume = Beep_Value[0];//获取音量
				AIQI_Play_Music(MyMusic2, Volume); //播放生日快乐歌
			}
	}
}
