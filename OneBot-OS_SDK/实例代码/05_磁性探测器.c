/**************************************************
 文件：Application.C
 介绍：Main Application
**************************************************/
#include "common.h"

int main( void )
{
	/*S1：变量初始化 */
	uint8_t HallValue[4]={0};
	uint16_t voltage_mv;
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_HALL);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	AIQI_IOConfig(IO4, IO_AI, 0, "adc1");
	AIQI_ADC_Open("adc1");
	/*S5: 模块运行 */
	while ( 1 )
	{	
		/*Your code*/
		voltage_mv=AIQI_AI_INPUT("adc1", IO4);//读取霍尔传感器电压值
		if(voltage_mv>3000)//电压大于3000感应到磁性
		{
			HallValue[1]=1;
		}else{
			HallValue[1]=0;
		}
		//发送给主控
		AIQI_SendDataToCC(HallValue);
		/*Your code end*/
	}
}
