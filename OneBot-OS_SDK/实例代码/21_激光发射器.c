/**************************************************
 文件：Application.C
 介绍：Main Application
**************************************************/
#include "common.h"

int main( void )
{
	/*S1：变量初始化 */
	uint8_t LE_Value=0;
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_LE);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	AIQI_IOConfig(IO5 , IO_OUT_PP, IO_LOW, "led1");//LE,初始低电平，灭
	/*S5: 模块运行 */
	while ( 1 )
	{	
		/*Your code*/
		if( AIQI_ReadDataFromCC(&LE_Value) != 0 )//读取主控发送的数据
		{
			AIQI_IO_OUTPUT("led1", (LE_Value | 0x00) ? IO_HIGH: IO_LOW);// 0-灭   1(!0)-亮		
		}	
		/*Your code end*/
	}
}
