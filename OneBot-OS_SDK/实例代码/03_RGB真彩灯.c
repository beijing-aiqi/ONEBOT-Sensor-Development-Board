/**************************************************
 文件：Application.C
 介绍：Main Application
**************************************************/
#include "common.h"

int main( void )
{
	/*S1：变量初始化 */
	uint8_t RGB_Value[4]={0};
	uint8_t duty=0;
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_RGB_LED);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	//配置并开启PWM
	AIQI_IOConfig(IO7, IO_PWM, 2000, "pwm1");// 频率=2000Hz
	AIQI_PWM_Open("pwm1");
	AIQI_IOConfig(IO6, IO_PWM, 200, "pwm2"); // 频率=200Hz
	AIQI_IOConfig(IO5, IO_PWM, 200, "pwm2");
	AIQI_PWM_Open("pwm2");
	// 设置占空比=100, 高电平，RGB熄灭
	//Red
	AIQI_PWM_Ctrl("pwm1",IO7,100);		
	//Green
	AIQI_PWM_Ctrl("pwm2",IO6,100);		
	//BLUE
	AIQI_PWM_Ctrl("pwm2",IO5,100);		
	/*S5: 模块运行 */
	while ( 1 )
	{	
		/*Your code*/
		if( AIQI_ReadDataFromCC(RGB_Value) != 0 )
		{
			//Red
			duty= 100- ( (RGB_Value[0] > 100) ? 100: RGB_Value[0]);// 获得的值取反，LED 低电平点亮
			AIQI_PWM_Ctrl("pwm1",IO7,duty);		
			//Green
			duty= 100- ( (RGB_Value[1] > 100) ? 100: RGB_Value[1]);// 获得的值取反，LED 低电平点亮
			AIQI_PWM_Ctrl("pwm2",IO6,duty);		
			//BLUE
			duty= 100- ( (RGB_Value[2] > 100) ? 100: RGB_Value[2]);// 获得的值取反，LED 低电平点亮
			AIQI_PWM_Ctrl("pwm2",IO5,duty);				
		}
		/*Your code end*/
	}
}
