/**************************************************
 文件：Application.C
 介绍：Main Application
**************************************************/
#include "common.h"

int main( void )
{
	/*S1：变量初始化 */
	uint8_t KeyValue[4]={0}; 
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_TILT);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	AIQI_IOConfig(IO4, IO_OUT_PP, IO_HIGH, "led1");
	AIQI_IOConfig(IO3, IO_IN_FLOAT, IO_HIGH, "key1");	
	/*S5: 模块运行 */
	while ( 1 )
	{	
		/*Your code*/
		if ( AIQI_IO_INPUT("key1") == IO_LOW )//读取水平仪状态
		{
			AIQI_DelayMS(10);
			if ( AIQI_IO_INPUT("key1") == IO_LOW )//再次读取水平仪状态 防抖
			{
				KeyValue[1] = 1;	//倾斜
				AIQI_IO_OUTPUT("led1", IO_LOW);// LED: 点亮							
			}
		}
		else
		{
			KeyValue[1] = 0;		//倾斜
			AIQI_IO_OUTPUT("led1", IO_HIGH);// LED: 熄灭				
		}
		KeyValue[0] = 0;
		KeyValue[2] = 0;
		//发送给主控
		AIQI_SendDataToCC(KeyValue);
		/*Your code end*/
	}
}
