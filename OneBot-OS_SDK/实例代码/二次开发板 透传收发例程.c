/**************************************************
 文件：Application.C
 介绍：Main Application
**************************************************/
#include "common.h"

int main( void )
{
	/*S1：变量初始化 */
	//收发数据缓存区
	uint8_t Data_FromCC[3]={0};
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_BOARD);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	
	/*S5: 模块运行 */
	while ( 1 )
	{	
	/*Your code begin*/
		if( AIQI_ReadDataFromCC(Data_FromCC) != 0 )       // 接收主控三个字节数据，通过wifi发送到PC
		{
			AIQI_SendDataToCC(Data_FromCC);
		}	
	/*Your code end*/
	}
}