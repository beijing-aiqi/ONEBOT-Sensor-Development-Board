/**************************************************
 文件：Application.C
 介绍：Main Application
**************************************************/
#include "common.h"

int main( void )
{
	/*S1：变量初始化 */
	uint8_t RRValue[4]={0}; 
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_RR);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	AIQI_IOConfig(IO1, IO_IN_FLOAT, IO_HIGH, "RR1");
	/*S5: 模块运行 */
	while ( 1 )
	{	
		/*Your code*/
		if ( AIQI_IO_INPUT("RR1") == IO_HIGH)
		{
			RRValue[1] = 0;	//0-IO_HIGE 磁感应有效、0-IO_LOW 未感应到
		}
		else
		{
				RRValue[1] = 1;	//1-IO_HIGE 磁感应有效、0-IO_LOW 未感应到
		}
		//发送给主控
		AIQI_SendDataToCC(RRValue);
		/*Your code end*/
	}
}
