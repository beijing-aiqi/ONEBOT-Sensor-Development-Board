/**************************************************
 文件：Application.C
 介绍：Main Application
**************************************************/
#include "common.h"

int main( void )
{
	/*S1：变量初始化 */
	uint8_t AP_Value[4]={0};
	float AP;
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_AP);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	//MS5611初始化
	AIQI_Atmos_Init();	
	/*S5: 模块运行 */
	while ( 1 )
	{	
		/*Your code*/
		AP = AIQI_Atmos_Read();//读取气压值
		//S3 Send to CC
		AP_Value[0]=(uint16_t)AP>>8;//高字节
		AP_Value[1]=(uint16_t)AP;//低字节
		AP_Value[2]=(uint16_t)(AP*100)%100;//两位小数
		//发送给主控
		AIQI_SendDataToCC(AP_Value);
		/*Your code end*/
	}
}
