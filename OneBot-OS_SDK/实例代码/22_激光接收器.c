/**************************************************
 文件：Application.C
 介绍：Main Application
**************************************************/
#include "common.h"

int main( void )
{
	/*S1：变量初始化 */
	uint8_t LRValue[4]={0}; 
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_LR);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	AIQI_IOConfig(IO1, IO_IN_FLOAT, IO_HIGH, "LR1");
	AIQI_IOConfig(IO5, IO_OUT_PP, IO_LOW, "SIGNAL1");
	/*S5: 模块运行 */
	while ( 1 )
	{	
		/*Your code*/
		if ( AIQI_IO_INPUT("LR1") == IO_LOW)
		{
			LRValue[1] = 1;	//即有激光照射时A0为低电平，无照射为高电平
			AIQI_IO_OUTPUT("SIGNAL1",IO_HIGH);
		}
		else
		{
			LRValue[1] = 0;	//
			AIQI_IO_OUTPUT("SIGNAL1",IO_LOW);
		}
		//发送给主控
		AIQI_SendDataToCC(LRValue);
		/*Your code end*/
	}
}
