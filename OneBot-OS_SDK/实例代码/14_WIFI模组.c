/**************************************************
 文件：Application.C
 介绍：Main Application
 由于SH340串口不能接多个设备，必须拆除WIFI传感器才能给公板烧入程序
**************************************************/
#include "common.h"

int main( void )
{
	/*S1：变量初始化 */
	//WIFI 名/密码
	static char* Router_SSID = "yourssid";
	static char* Router_PassWord = "yourpwd";
	//pc端 ip地址 端口号
	static char* PC_IP = "ServerIP";
	static char* PC_ComNum = "ServerComNum";
	//收发数据缓存区
	uint8_t Data_FromCC[3]={0};
	char Data_FromWIFI[15]={0};
	char Dat_Cache_String[10]={0};
	//连接状态
	uint8_t Connected=0;
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_WIFI);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	//ESP8266- RST
	AIQI_IOConfig(IO3, IO_OUT_PP, IO_HIGH, "ESP_RST");
	//UART
	AIQI_Interface_Init_UART("uart1", OS_BAUD_RATE_115200);
	//设置WiFi模块工作模式
 	AIQI_WIFI_Net_Mode_Choose(STA);
	AIQI_DelayMS(1000);//等待模式设置完成
	/*S5: 模块运行 */
	while ( 1 )
	{	
		if(Connected == 0)    //初次使用尚未连接wifi
		{
			Connected = AIQI_WIFI_StaTcpClient_Connect(Router_SSID, Router_PassWord,PC_IP, PC_ComNum);   // 连接指定路由器和IP
			if(Connected) AIQI_WIFI_StaTcpClient_SendString("\r\n I am online:\r\n");
			AIQI_DelayMS(5000);
		}
		else
		{
			if( AIQI_ReadDataFromCC(Data_FromCC) != 0 )       // 接收主控三个字节数据，通过wifi发送到PC
			{
				sprintf(Dat_Cache_String,"Recv:%d,%d,%d\r\n",Data_FromCC[0],Data_FromCC[1],Data_FromCC[2]);//将接收到的数据转换为字符串
				AIQI_WIFI_StaTcpClient_SendString(Dat_Cache_String);			//将字符串发送给wifi模块
			}	
			if( AIQI_WIFI_StaTcpClient_RecvString(Data_FromWIFI,0) != 0)			//接收PC通过wifi发来的数据，发送到主控 次数据为ascii码
			{
				AIQI_SendDataToCC((uint8_t *)Data_FromWIFI);
			}
		}
			/*Your code end*/
	}
}

