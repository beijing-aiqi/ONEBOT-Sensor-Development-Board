/**************************************************
 文件：Application.C
 介绍：Main Application
 		(1)	传感器通电后，需要预热20S左右，测量的数据才稳定，
			传感器发热属于正常现象，因为内部有电热丝，如果烫手就不正常了
		(2) 平面烟雾气体传感器（MP-2），在较宽的浓度范围内对丙烷、 烟雾等有良好的灵敏度，
			当环境空气中有被检测气体存在时传感器电导率发生变化，该气体的浓度越高，传感器的
			电导率就越高。采用简单的电路即可将这种电导率的变化转换为与气体浓度对应的输出信
			号。当精确测量时，还需考虑温湿度的影响.
			曲线拟合公式(C函数没有开方函数，原数据图形xy轴调换，调换后的x轴统一减去3.5再取
			绝对值)：f(x)=a*x^b+c
			a=-3291;  b=-1.491;  c=-1050
		(3) 参考：
			在正常环境中，即：没有被测气体的环境，
			设定传感器输出电压值为参考电压，这时，A0端的电压在1V左右，
			当传感器检测到被测气体时，具体电压值对应气体浓度详细参数建议参考官方手册。
			注意：如果您是用来做精密仪器，请购买市场上标准的校准仪器，不然存在误差，
			因为，输出浓度和电压关系的比值并非线性，而是趋于线性。
**************************************************/
#include "common.h"

/*Your function*/
uint16_t Read_Smoke()
{
	float a=3291;
	float b=-1.491;
	float c=-1050;
	float x=0;
	float y=0;//f(x)
	float z=0;
	z=AIQI_AI_INPUT("adc0", IO1);//获取传感器测量端电压值，(0~3300 mV)
	//z=2000;//测试数值  z=0x72f0
	if(z>3100)//拟合算法最大支持数值建议为3100，对应气体ppm数值为11851。
	{
		z=3100;
	}
	x=abs(z-3500)*0.001;
	y=a*(pow(x,b))+c;   //曲线拟合公式：f(x)=a*x^b+c
	return (uint16_t)y;
}
/*Your function end*/

int main( void )
{
	/*S1：变量初始化 */
	uint8_t ST_Value[4]={0};
	uint16_t st=0;
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_ST);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	AIQI_IOConfig(IO1, IO_AI, 0, "adc0");
	AIQI_ADC_Open("adc0");
	/*S5: 模块运行 */
	while ( 1 )
	{	
		/*Your code*/
			st = Read_Smoke(); //读取烟雾浓度
			
			ST_Value[2]=0;
			ST_Value[1]=st & 0xff;
			ST_Value[0]=st >> 8;
			
			//发送给主控
			AIQI_SendDataToCC(ST_Value);
		/*Your code end*/
	}
}
