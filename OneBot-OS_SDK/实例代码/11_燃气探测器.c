/**************************************************
 文件：Application.C
 介绍：Main Application
 		(1)	传感器通电后，需要预热20S左右，测量的数据才稳定，
			传感器发热属于正常现象，因为内部有电热丝，如果烫手就不正常了
		(2) 天然气检测传感器，对不同种类、不同浓度的气体有不同的电阻值。
			如果要实现准确测量，需要用5000ppm甲烷校准传感器
			当精确测量时，还需考虑温湿度的影响
		(3) 参考：
			在正常环境中，即：没有被测气体的环境，
			设定传感器输出电压值为参考电压，这时，AOUT端的电压在1V左右，
			当传感器检测到被测气体时，电压每升高0.1V，实际被测气体的浓度增加200ppm
			（简单的说：1ppm=1mg/kg=1mg/L=1×10-6 常用来表示气体浓度，或者溶液浓度。）
			注意：如果您是用来做精密仪器，请购买市场上标准的校准仪器，不然存在误差，
			因为，输出浓度和电压关系的比值并非线性，而是趋于线性。
**************************************************/
#include "common.h"

#define GAS_VREF	900  //在正常环境中，AOUT输出电压

int main( void )
{
	/*S1：变量初始化 */
	uint8_t GasValue[4]={0};
	uint16_t voltage_mv = 0;
	
	uint16_t voltage_sample[10] = {0};
	uint16_t voltage_min=0;
	uint16_t voltage_max=0;
	uint16_t gas=0;
	uint8_t  i=0;
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_GAS);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	AIQI_IOConfig(IO3, IO_AI, 0, "adc1");
	AIQI_ADC_Open("adc1");

	/*S5: 模块运行 */
	while ( 1 )
	{	
		/*Your code*/
			for(i=0;i<10;i++)
			{
				voltage_sample[i]=AIQI_AI_INPUT("adc1", IO3);//读取十次燃气传感器电压值
			}
			//find max
			voltage_max = voltage_sample[0];
			for(i=1;i<10;i++)//循环获得十次中的最大值
			{
				if(voltage_max < voltage_sample[i])
					voltage_max=voltage_sample[i];
			}
			//find min
			voltage_min = voltage_sample[0];
			for(i=1;i<10;i++)//循环获得十次中的最小值
			{
				if(voltage_min > voltage_sample[i])
					voltage_min=voltage_sample[i];
			}
			//Sum
			for(i=0;i<10;i++)//循环获得十次之和
			{
				voltage_mv += voltage_sample[i];
			}
			voltage_mv -= voltage_max;//对参数和进行计算
			voltage_mv -= voltage_min;
			voltage_mv /=8;
			if(voltage_mv < GAS_VREF)//判断参数
				gas=300;
			else
				gas=300+(voltage_mv-GAS_VREF)*2; //对参数进行计算
			//发送给主控
			GasValue[2]=0;
			GasValue[1]=gas & 0xff;
			GasValue[0]=gas >> 8;
			AIQI_SendDataToCC(GasValue);
			/*Your code end*/
	}
}
