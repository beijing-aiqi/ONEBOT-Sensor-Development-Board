/**************************************************
 文件：Application.C
 介绍：Main Application
**************************************************/
#include "common.h"

int main( void )
{
	/*S1：变量初始化 */
	uint8_t LightValue[4]={0};
	uint16_t voltage_mv = 0;
	uint16_t light_cur=0;
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_LIGHT);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	AIQI_IOConfig(IO3, IO_AI, 0, "adc1");
	AIQI_ADC_Open("adc1");
	/*S5: 模块运行 */
	while ( 1 )
	{	
		/*Your code*/
		voltage_mv=AIQI_AI_INPUT("adc1", IO3);//读取光敏电阻的电压
		light_cur = voltage_mv*10/33;//将读取到的电压进行转换
		LightValue[1]=light_cur & 0xff;
		LightValue[0]=light_cur >> 8;
		AIQI_SendDataToCC(LightValue);//发送给主控
		/*Your code end*/
	}
}
