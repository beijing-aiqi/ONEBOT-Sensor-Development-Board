/**************************************************
 文件：Application.C
 介绍：Main Application
**************************************************/
#include "common.h"

int main( void )
{
	/*S1：变量初始化 */
	uint8_t ledValue=0;
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_LED_8C);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	AIQI_IOConfig(IO5 , IO_OUT_PP, IO_HIGH, "led1");//红
	AIQI_IOConfig(IO6 , IO_OUT_PP, IO_HIGH, "led2");//绿
	AIQI_IOConfig(IO7 , IO_OUT_PP, IO_HIGH, "led3");//蓝
	AIQI_IOConfig(IO8 , IO_OUT_PP, IO_HIGH, "led4");//黄
	AIQI_IOConfig(IO3 , IO_OUT_PP, IO_HIGH, "led5");//暖白
	AIQI_IOConfig(IO4 , IO_OUT_PP, IO_HIGH, "led6");//棕
	AIQI_IOConfig(IO10, IO_OUT_PP, IO_HIGH, "led7");//白
	AIQI_IOConfig(IO9 , IO_OUT_PP, IO_HIGH, "led8");//紫
	/*S5: 模块运行 */
	while ( 1 )
	{	
		/*Your code*/
		if( AIQI_ReadDataFromCC(&ledValue) != 0 )
			{
				AIQI_IO_OUTPUT("led1", (ledValue & 0x01) ? IO_LOW: IO_HIGH);//红 0-灭 1-亮		
				AIQI_IO_OUTPUT("led2", (ledValue & 0x02) ? IO_LOW: IO_HIGH);//绿 0-灭 1-亮 	
				AIQI_IO_OUTPUT("led3", (ledValue & 0x04) ? IO_LOW: IO_HIGH);//蓝 0-灭 1-亮		
				AIQI_IO_OUTPUT("led4", (ledValue & 0x08) ? IO_LOW: IO_HIGH);//黄 0-灭 1-亮 	
				AIQI_IO_OUTPUT("led5", (ledValue & 0x10) ? IO_LOW: IO_HIGH);//暖白 0-灭 1-亮		
				AIQI_IO_OUTPUT("led6", (ledValue & 0x20) ? IO_LOW: IO_HIGH);//棕 0-灭 1-亮 	
				AIQI_IO_OUTPUT("led7", (ledValue & 0x40) ? IO_LOW: IO_HIGH);//白 0-灭 1-亮		
				AIQI_IO_OUTPUT("led8", (ledValue & 0x80) ? IO_LOW: IO_HIGH);//紫 0-灭 1-亮 	
			}
			/*Your code end*/
	}
}
