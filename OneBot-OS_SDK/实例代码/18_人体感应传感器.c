/**************************************************
 文件：Application.C
 介绍：Main Application
**************************************************/
#include "common.h"

int main( void )
{
	/*S1：变量初始化 */
	uint8_t PIRValue[4]={0}; 
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_PIR);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	AIQI_IOConfig(IO1, IO_IN_FLOAT, IO_HIGH, "pir1");
	/*S5: 模块运行 */
	while ( 1 )
	{	
		/*Your code*/
		if ( AIQI_IO_INPUT("pir1") == IO_HIGH)
		{
			PIRValue[1] = 1;	//1-IO_HIGE 红外感应有效、0-IO_LOW 未感应到
		}
		else
		{
			PIRValue[1] = 0;	//1-IO_HIGE 红外感应有效、0-IO_LOW 未感应到
		}
		//发送给主控
		AIQI_SendDataToCC(PIRValue);
		/*Your code end*/
	}
}
