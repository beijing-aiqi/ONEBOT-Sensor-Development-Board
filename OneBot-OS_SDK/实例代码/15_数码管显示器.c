/**************************************************
 文件：Application.C
 介绍：Main Application
**************************************************/
#include "common.h"

int main( void )
{
	/*S1：变量初始化 */
	uint16_t Num = 0;	// 数码管要显示的数字
	uint8_t DigitalNum[2]= {0}; //数字范围0-999 所以由两位uint8存储
	/*S2: 模块选择 */
	AIQI_Module_Select(ONEBOT_DIG_DISP);//模块名称见说明文档或common.h
	/*S3: 系统初始化 */
	AIQI_InterfaceCC(1);
	/*S4：模块初始化*/
	AIQI_Num_Display_Init();
	AIQI_Num_DisPlay_Level(5);//设置亮度等级并使能（1-8）
	
	/*S5: 模块运行 */
	while ( 1 )
	{	
		/*Your code*/
		if( AIQI_ReadDataFromCC(DigitalNum) != 0 )//获取主控发送的数据
		{
			Num = DigitalNum[0];
			Num <<=8;
			Num +=DigitalNum[1];
			AIQI_Num_DisPlay_CLR();//清屏
			AIQI_Num_DisPlay_ThreeBit(Num);//数码管显示三位数字
		}
		/*Your code end*/
	}
}
