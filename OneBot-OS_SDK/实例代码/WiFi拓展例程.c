#include "common.h"

int main( void )
{
  /*S1：变量初始化 --------------------------------------------------*/
  //WIFI 名/密码
  static char* SSID= "yourssid";
  static char* Password = "yourpwd";
  //OneBot云用户名/密码
  static char* UserName= "yourname";
  static char* UserPWD= "yourpwd";
  //收发数据缓存区
  uint8_t ToWiFi_Data[3]= {0};
  uint8_t ToCC_Data[3]= {0};
  char Name[30]="";
  uint32_t GetDate;
  /*S2: 模块选择 --------------------------------------------------*/
  AIQI_Module_Select(ONEBOT_WIFI);//模块名称见说明文档或common.h
  /*S3: 系统初始化 --------------------------------------------------*/
  AIQI_InterfaceCC(1);
  /*S4：模块初始化--------------------------------------------------*/
  //ESP8266- RST
  AIQI_IOConfig(IO3, IO_OUT_PP, IO_HIGH, "ESP_RST");
  //UART
  AIQI_Interface_Init_UART("uart1", OS_BAUD_RATE_115200);
  //设置ESP8266工作模式
  AIQI_WIFI_Net_Mode_Choose(STA);
  /*S5: 模块运行 --------------------------------------------------*/
  while ( 1 )
  {
    /*Your code------------------------------------------------------*/
    if(AIQI_WIFI_JoinAP(SSID, Password ,6000) == 1)//连接wifi
    {
      // 接收主控数据，通过WiFi发送到OneBot云
      if( AIQI_ReadDataFromCC(ToWiFi_Data) != 0 )//读取主控发送的数据到数组toWiFi_Data
      {
        switch (ToWiFi_Data[0])
        {
					//以主控发送的第0个字节做为设备名标识，第1个字节作为数据(可以自主分配)
					//注意设备名不可为中文，数据只能为int
					case 1: OneBotYunSet(UserName,UserPWD,"temperature",ToWiFi_Data[1]);//将数据发送到OneBot云          break;
					case 2: OneBotYunSet(UserName,UserPWD,"humidity",ToWiFi_Data[1]);//将数据发送到OneBot云平台
          break;
        }
      }
      GetDate= OneBotYunGet(UserName,UserPWD,Name);//接收OneBot云发送来的数据和设备名，
      if(strstr(Name,"RGB")) //如果接收到的设备名包含“RGB”
      {
        ToCC_Data[0] = GetDate/1000/1000;//取出第一字节
        ToCC_Data[1] = GetDate/1000%1000;//取出第二字节
        ToCC_Data[2] = GetDate%1000;//取出第三字节
        AIQI_SendDataToCC(ToCC_Data);//发送到主控
      }
    }
    /*Your code end-----------------------------------------------------*/
  }
}
