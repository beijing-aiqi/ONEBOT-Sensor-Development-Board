#ifndef __STM32F030_UART_H
#define __STM32F030_UART_H

#include "DR_COM.H"
#include "DM_UART.H"
#include "HAL.H"
#include "stm32f0xx.h"


extern os_status_t DR_UART_DRIVERREGISTER(char *interface, os_uart_pin_t uart_pin, os_uart_configure_t uart_configure);

#endif

