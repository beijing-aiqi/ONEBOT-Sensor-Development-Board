#ifndef __PUBLIC_H
#define __PUBLIC_H


#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "math.h"
#include <stdbool.h> // WiFi Module

#include "stm32f0xx.h"
#include "STM32F030_UART.H"
#include "STM32F030C8_TIMER.H"
#include "STM32F030C8_GPIO.H"
#include "STM32F030C8_PWM.H"
#include "STM32F030C8_ADC.H"
#include "STM32F030C8_IIC.H"
#include "STM32F030C8__INTERNAL_FLASH.H"

#include "ASSY_Reverse.h"

#include "HAL.H"
#include "Device.H"
#include "system.H"
#include "protocol.H"

#include "ASSY_RUBBERBANDS.H"
#include "BSP_Init.H"

#include "../../Components/Kernel/Include/DM_PWM.H"


#include "BSP_Init.h"
#include "BEEP.h"
#include "MS5611.h"
#include "AHT10.h"
#include "SHT21.h"
#include "esp8266.h"
#include "Aip650.h"


#endif
