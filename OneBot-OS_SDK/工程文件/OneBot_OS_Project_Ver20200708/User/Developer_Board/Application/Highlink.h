#ifndef __HIGHLINK_H
#define __HIGHLINK_H
#include "stm32f0xx.h"

//Ȩ�����
#define HighLink_BASE_NUM 10


#define Port_USART1_NUM     10
#define Port_USART2_NUM     20
#define Port_USART3_NUM     30
#define Port_USART4_NUM     40
#define Port_USART5_NUM     50
#define Port_USART6_NUM     60

#define Port_SPI1_NUM     60
#define Port_SPI2_NUM     70
#define Port_SPI3_NUM     80




#define Port_IIC1_NUM     90
#define Port_IIC2_NUM     100

#define Port_USART1_UpdateNUM     80
#define Port_USART2_UpdateNUM     90
#define Port_USART3_UpdateNUM     100
#define Port_USART4_UpdateNUM     110
#define Port_USART5_UpdateNUM     120
#define Port_USART6_UpdateNUM     130

#define Port_SPI1_UpdateNUM     140
#define Port_SPI2_UpdateNUM     30
#define Port_SPI3_UpdateNUM     160

#define Port_IIC1_UpdateNUM     160
#define Port_IIC2_UpdateNUM     170


#define Port_USART1_OVERTIME_NUM  100
#define Port_USART2_OVERTIME_NUM  100

typedef struct
{
    uint32_t Temp_Cnt;
    uint32_t Clk_Base;
    uint32_t Run_Flag;
    uint32_t USART1CLK_Cnt;
    uint32_t USART2CLK_Cnt;
    uint32_t USART3CLK_Cnt;
    uint32_t USART4CLK_Cnt;
    uint32_t USART5CLK_Cnt;
    uint32_t USART6CLK_Cnt;
    uint32_t SPI1CLK_Cnt;
    uint32_t SPI2CLK_Cnt;
    uint32_t SPI3CLK_Cnt;
    uint32_t IIC1CLK_Cnt;
    uint32_t IIC2CLK_Cnt;

    uint32_t USART1UpdateCLK_Cnt;
    uint32_t USART2UpdateCLK_Cnt;
    uint32_t USART3UpdateCLK_Cnt;
    uint32_t USART4UpdateCLK_Cnt;
    uint32_t USART5UpdateCLK_Cnt;
    uint32_t USART6UpdateCLK_Cnt;
    uint32_t SPI1UpdateCLK_Cnt;
    uint32_t SPI2UpdateCLK_Cnt;
    uint32_t IIC1UpdateCLK_Cnt;
    uint32_t IIC2UpdateCLK_Cnt;

} Highlink_Type;

//extern Highlink_Type Highlink_Infor;

//#define Log_RxBuf_Max   255
//typedef struct
//{
//  uint8_t RxBuf[Log_RxBuf_Max];
//
//} Log_Type;
//extern Log_Type Log_Infor;


//uint32_t Highlink_RunDispose(void);
//uint32_t HighLink_Main(void);
//void BroadcastExceptSelf(void);
//void RxDataDispose(void);


#endif

