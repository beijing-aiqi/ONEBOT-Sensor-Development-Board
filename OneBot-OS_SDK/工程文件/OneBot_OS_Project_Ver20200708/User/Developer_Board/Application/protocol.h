#ifndef __PROTOCOL_H
#define __PROTOCOL_H
//#include "Public.h"

#include "common.h"//lty

//Global Variant ---------------------------------------------------------------
extern uint8_t RxNum;
extern uint8_t RxState;
extern uint8_t Dispose_data(uint8_t *buf, uint8_t *cnt_pt);

//extern uint32_t LED_STAT;

extern uint8_t g_Rx_Data[3];  //wpbadd: D2,D1,D0, 传感板<--主控
extern uint8_t g_Tx_Data[3];  //wpbadd: D2,D1,D0, 传感板-->主控


//Macro Define -----------------------------------------------------------------

//wpbadd: for Service_infor.Service_Stat
typedef enum
{
	//方法相关 1-6
	STAT_SN_Error = 1,
	STAT_SN_OK,
	STAT_TYPE_Error,
	STAT_TYPE_OK,
	STAT_MID_Error,
	STAT_MID_OK,

	//表项存在状态 7-9
	STAT_EXIST,
	STAT_CHANGE,
	STAT_INEXISTENCE,

	//过程相关 10-12
	STAT_START,
	STAT_END,
	STAT_ING,

	//收发数据相关 13-15
	STAT_SendData_Succeed,
	STAT_ReceiveData_Succeed,
	STAT_ACK_OK,

	//---------------
	//错误状态 16-21
	STAT_Assert_Error,
	STAT_SendData_Failure,
	STAT_ReceiveData_NO,
	STAT_ReceiveData_XORError,
	STAT_ACK_NO,
	STAT_ReceiveData_Error,

	//根据协议处理返回的状态信息，作出不同处理 22-26
	Service_Stat_MethodVerify,
	Service_Stat_VRFWAITVerify,
	Service_Stat_Repeater,                  //转发
	Service_Stat_No_Reply,                  //不回复
	Service_Stat_Reply,                 //回复

	//  Service_Stat_LEDONOFF,
	//27-
	Service_Stat_M1_BEEP_SET,   //wpb add
	Service_Stat_M2_LED8C_SET,  //wpb add
	Service_Stat_M3_RGBLED_SET, //wpb add

	Service_Stat_M9_TILT_GET, //wpb add

	Service_Stat_M14_WiFi_SET, //wpb add
	Service_Stat_M15_RELAY_SET, //wpb add
	Service_Stat_M16_DIGDISP_SET, //wpb add
	
	//二批传感器
	Service_Stat_M21_LE_SET, //
	
	


} STAT_VALUE;

#if 0
//错误状态
#define  STAT_Assert_Error          1024
#define  STAT_SendData_Failure    1025
#define  STAT_ReceiveData_NO      1026
#define  STAT_ReceiveData_XORError  1027
#define  STAT_ACK_NO                1028
#define  STAT_ReceiveData_Error     1029
#endif

#define Service_RxBuf_MaxSize        255
#define Service_SendBuf_MaxSize      255
#define Service_Self                 0
#define Service_FirstMethod          5  //wpb: 按照协议格式，第5个(从0开始)为 第一个方法格式

#if 0
//根据协议处理返回的状态信息，作出不同处理
#define Service_Stat_MethodVerify           1
#define Service_Stat_VRFWAITVerify          2
#define Service_Stat_Repeater               3  //转发
#define Service_Stat_No_Reply               4  //不回复
#define Service_Stat_Reply              5  //回复
#define Service_Stat_LEDONOFF             6  //
#endif

#define LED_STAT_ON   1
#define LED_STAT_OFF  0

//wpb: 方法列表
typedef enum
{

	//方法  方法含义  参数个数  参数范围  备注
	//1 电机速度  1 0-255 设置电机速度
	METHOD_MOTOR_Speed = 1,

	//2 指定具体电机速度  2   第一个参数指定具体电机，第二个参数设置电机速度
	METHOD_MOTOR_xSpeed,

	//3 电机转动圈数  1 0-255 指定电机转动圈数
	METHOD_MOTOR_CircleNum,

	//4 指定具体电机圈数  2   第一个参数指定具体电机，第二个参数设置电机圈数
	METHOD_MOTOR_xCircleNum,

	//方法  方法含义  参数个数  参数范围  备注
	//特殊方法说明
	//255=0xFF  广播查询  0   收到查询信息后，一次性回复所有参数信息，用于传感器方法数据的回复
	METHOD_BROAD_Query = 0xFF, //255

	//254=0xFE  确认报文  1 1：仅表示收到信息
	METHOD_MSG_Ack = 0xFE, //254 //2：表示询问的信息不存在
	//3：表示询问的信息存在 仅用于确认收到信息，不回复具体内容。


	//wpb add start---------------------------------------------

//	//5 温度  1
//	METHOD_TEMP = 5,
//	//6 湿度  1
//	METHOD_HUMI,

	//7 霍尔  1
	METHOD_HALL=7,

	//8 光敏  2: 高位字节  低位字节
	METHOD_LIGHT,

	//9 倾斜开关  1
	METHOD_TILT,

	//10 振动   1
	METHOD_SHAKE,

	//11 天然气 2: 高位字节  低位字节
	METHOD_GAS,

	//12 光电开关 1
	METHOD_OPTO_KEY,

	//13 触摸开关 1
	METHOD_TOUCH_KEY,

	//14 蜂鸣器 1
	METHOD_BEEP,

	//15 LED  1
	METHOD_LED,

	//16 数码管 2: 高位字节  低位字节
	METHOD_DIG_DISP,

	//17 继电器 1
	METHOD_RELAY,

	//18 WiFi 访问数据 3
	METHOD_WIFI_GetData,
	//19 WiFi 控制数据 3
	METHOD_WIFI_CtrlData,

	//wpb add end ---------------------------------------------
	
	//二批传感器新增 start-----------------------------------------
	//26 红外热释电 1
	METHOD_PIR = 26,
	
	//27 烟雾 3
	METHOD_ST,
	
	//28 干簧管 1
	METHOD_RR,
	
	//29 激光发射 1
	METHOD_LE,
	
	//30 激光接收 1
	METHOD_LR,
	
	//31 大气压强 3
	METHOD_AP,
	//32 海拔高度 3
	//暂无
	
	//5 温度 1
	METHOD_TH_T = 5,
	//6 湿度 1
	METHOD_TH_H = 6,

	
	//二批传感器新增 end-----------------------------------------


	//激光传感器相关
	//49  激光传感器测距，单位mm  2 0~1000
	METHOD_LASER_mm = 49,

	//50  激光传感器测距，单位cm  1 0~100
	METHOD_LASER_cm,


	//颜色传感器相关
	//51  颜色  1   0-无色、1-蓝、2-红、3-黄、4-白、5-绿、6-黑、8-灰、12-紫
	METHOD_Color,

	//52  灰度  1 0~100
	METHOD_Gray,

	//53  RGB 3 每个字节0~255
	METHOD_RGB,

	//54  环境光强度  2 0~1000
	METHOD_ENV_Light,

	//火焰传感器相关
	//55  识别火焰  1   0-无、1-左二、2-左一、3-中、4-右一、5-右二
	METHOD_FIRE_Recongize,

	//56  火焰强度  1 0~100
	METHOD_FIRE_Grade,

	//指南针传感器相关
	//57  磁场角度  2 0~360°
	METHOD_MAG_Angle,

	//六轴传感器相关
	//58  骰子点数
	METHOD_DICE_Num,

	//传感器开发板相关
	//59  开发板控制数据  3 每个字节0~255
	METHOD_BOARD_CtrlData,

	//60  开发板访问数据  3 每个字节0~255
	METHOD_BOARD_GetData,

	//舵机相关
	//61  角度控制  2 -160°~160°
	METHOD_STEER_AngleCtrl,

	//62  功率控制  1 -100~100
	METHOD_STEER_PowerCtrl,


	//注册相关
	//75  设备端口号  1
	METHOD_REG_DevicePort = 75,

	//76  回应版本号  4
	METHOD_REG_SotfVer,

	//77  回应方法      具体参数个数根据方法本身的字节个数和设备本身所含有的方法个数决定
	METHOD_REG_Method,

	//78  回应SN  7
	METHOD_REG_SN,

	//79  回应设备类型  1
	METHOD_REG_DeviceType,

	//80  回应设备注册号  1
	METHOD_REG_RegNum,

	//81  回应管理节点设备类型  1
	METHOD_REG_MNode_DeviceType,

	//82  回应管理节点注册号  1
	METHOD_REG_MNode_RegNum,

	//83  询问设备端口号  0
	METHOD_REG_ASK_DevicePort,

	//84  询问版本号  0
	METHOD_REG_ASK_SoftVer,

	//85  询问方法  0
	METHOD_REG_ASK_Method,

	//86  询问SN  0
	METHOD_REG_ASK_SN,

	//87  询问设备类型  0
	METHOD_REG_ASK_DeviceType,

	//88  询问注册号  0
	METHOD_REG_ASK_RegNum,

	//89  询问管理节点设备类型  0
	METHOD_REG_ASK_MNode_DeviceType,

	//90  询问管理节点注册号  0
	METHOD_REG_ASK_MNode_RegNum,

	//91  询问注册号是否已经分配  0
	METHOD_REG_ASK_IS_RegNumSet,

	//92  删除设备注册信息  0   删除指定还是广播，通过目的地址来确定
	METHOD_REG_DEL_RegInfor,

	//93  确认SN  7
	METHOD_REG_ACK_SN,

	//94  固件恢复完成  0
	METHOD_REG_FM_RECOV_OK,

	//95  查找传感器  0
	METHOD_REG_SEARCH_Sensor,


	//97  询问注册地址  0
	METHOD_REG_ASK_RegAddr = 97,

	//98  回复注册地址  1
	METHOD_REG_RegAddr,

	//99  主动注册  0
	METHOD_REG_SubmitReg,


	//LED灯相关
	//106 灯亮度控制  1 0~255
	METHOD_LED_LightCtrl = 106,

	//107 指定某个灯亮度  2 第一个参数是具体灯编号，第二个参数是亮度
	METHOD_LED_xLightCtrl,

	//108 舵机角度读取  2
	METHOD_STEER_AngleRead,

	//109 按键状态  1   //SensorBoard: Key module
	METHOD_KEY_State,

	//110 球传感器方向  1
	METHOD_BALL_Dir,

	//111 球信号强度  1
	METHOD_BALL_DB,

	//112 加速度X 3 -2000.00cm/s*s~2000.00cm/s*s
	METHOD_ACC_X,

	//113 加速度Y 3 -2000.00cm/s*s~2000.00cm/s*s
	METHOD_ACC_Y,

	//114 加速度Z 3 -2000.00cm/s*s~2000.00cm/s*s
	METHOD_ACC_Z,

	//115 被攻击角度  1 0-无、1-左二、2-左一、3-右一、4-右二
	METHOD_ATTACK_Angle,

	//116 攻击开关  1 0~255
	METHOD_ATTACK_ONOFF,


	//117 RGB LED
	//117 点灯  3
	METHOD_RGB_LED,

	//音效相关
	//150 播放音效  1 0~255 Jundong 20190118
	METHOD_MUSIC = 150,


	//时间相关
	//160 延时  1 0~255 100ms为单位    Jundong
	METHOD_DELAY = 160,


} METHOD_DEFINE;

//Struct Define ----------------------------------------------------------------

//wpb: 帧格式
typedef struct
{
	uint8_t  RxBuf[Service_RxBuf_MaxSize];
	uint8_t  SendBuf[Service_SendBuf_MaxSize];
	uint8_t  SendPt;    //wpb: 发送缓存区指针


	uint8_t  Length;    //wpb: 一帧数据，总的长度
	uint8_t  Length_Remain; //wpb: 抛去5字节的协议头后，剩余的数据长度
	uint8_t  Service_Format;
	uint8_t  NID_Flag;
	uint8_t  Urgency;
	uint8_t  ACK;
	uint8_t  Dest_adr;
	uint8_t  RxPtr;     //wpb: 接收缓冲器 指针
	uint8_t  MethodFormat;  //wpb: 方法格式
	uint8_t  Method_Cnt;
	uint32_t Method;
	uint8_t  Method_Remain;
	uint8_t  MethodLength;  //wpb: 方法长度: 方法格式的bit7-bit6
	uint32_t Service_Stat;


	uint8_t  Param_Flag;
	uint8_t  Param_Length;  //wpb: 参数个数,方法格式的bit5-bit0,范围: 0-50,63(仅限固件更新使用)

	uint16_t Temp_Service;
	uint32_t Service_SendStat;

	uint32_t Service_RxStat_Current;   //针对SPI，IIC等的实时状态
	uint8_t  TempPortType;
	uint32_t TempWeight;
} Service_Typedef;
extern Service_Typedef Service_infor;


#define Uart_Tx_MaxSize       100   

//#ifdef Mx_14_WIFI
//#define Uart_Rx_MaxSize       500	//lty:如果过小wifi模块接收数据不全
//#else
#define Uart_Rx_MaxSize       500	
//#endif

typedef struct ModuleInfor
{
	uint8_t *TxPtr;
	uint8_t TxNum;
	uint8_t TxCnt;
	uint32_t TxState;
	uint8_t TxBuf[Uart_Tx_MaxSize];
	uint8_t *RxPtr;
	uint8_t RxNum;
	uint8_t RxCnt;
	uint32_t RxState;
	uint8_t RxBuf[Uart_Rx_MaxSize];

	//ESP8266
	uint8_t FramFinishFlag;                                // 15  //帧数据结束标识
	
} ModuleInfor_Typedef;

extern ModuleInfor_Typedef  Uart1_Infor;
//extern ModuleInfor_Typedef  Uart2_Infor;
extern ModuleInfor_Typedef RB1_Infor;


//wpb add
typedef struct
{
	uint8_t  reg_num;       //register number 注册号码
	uint8_t  reg_num_Flag;  //register number set Flag: 0-not set; 1- have set 注册号标志:0-没有设置;1 -设置
	uint8_t  sn[7 +
	            1];       //device serial number:  [0..6]:SN data; [7]: Flash Write Flag 设备序列号 [0 . .6]:SN数据;[7]: Flash写标志
	uint8_t  soft_version[4 +
	                      1]; //software version   :  [0..3]:SoftVersion data; [4]: Flash Write Flag 软件版本
	uint8_t  device_type; //设备类型
} DeviceInfor_Typedef;

extern DeviceInfor_Typedef Device_Infor;


//Function ---------------------------------------------------------------------
void Service_Fill(uint32_t service, uint8_t method, uint32_t param);
uint32_t Dispose_Method(void);
uint8_t Dispose_Protocol(void);
uint8_t Fill_Method(uint32_t method, uint8_t *param, uint8_t methodlength,
                    uint8_t paramlength);
uint8_t XOR_Check(uint8_t *bufpt, uint16_t num);



#endif



