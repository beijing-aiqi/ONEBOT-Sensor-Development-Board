#ifndef __SYSTEM_H
#define __SYSTEM_H

#include "stm32f0xx.h"
#include "protocol.h"

//#define Sys_Nid_Auto

#define NO    0
#define YES   1
#define NONE  2

#if 0
extern uint8_t  SN[8];	//wpb: 设备SN
extern uint8_t  TYPE;	//wpb: 设备类型
extern uint8_t  MID;	//wpb: 设备注册号
extern uint8_t  MID_Flag;
#endif

#define SYS_ID_MAXSIZE         7

#if 0
//yuanlai
//表项存在状态
#define STAT_EXIST          6
#define STAT_CHANGE         7
#define STAT_INEXISTENCE    8

//过程相关
#define STAT_START          9
#define STAT_END            10
#define STAT_ING            11

//收发数据相关
#define STAT_SendData_Succeed        12
#define STAT_ReceiveData_Succeed     13
#define STAT_ACK_OK                  14

//方法相关
#define STAT_SN_Error             1
#define STAT_SN_OK                2
#endif




typedef struct
{
    uint8_t RegisterNum;
    uint32_t VRF_Stat;
    uint8_t  PortSn;  //系统中端口的总个数
    uint8_t  PortType;
    uint8_t S_Addr[SYS_ID_MAXSIZE];
    uint8_t D_Addr[SYS_ID_MAXSIZE];
    uint32_t TimeGap;
    uint32_t PortStat;
    uint32_t NodeStat;
    uint32_t TempStat;
} Sys_Typedef;

extern Sys_Typedef Sys_Infor;


//转发表  接口和节点的关系
#define PortType_USART1         1
#define PortType_USART2         2
#define PortType_USART3         3
#define PortType_USART4         4
#define PortType_USART5         5
#define PortType_USART6         6

#define PortType_SPI1           11
#define PortType_SPI2           12
#define PortType_SPI3           13

#define PortType_IIC1           21

#define PortType_Error          30



typedef struct Node
{
    uint8_t NID[SYS_ID_MAXSIZE];
    uint32_t Sn;
    uint32_t State;
    uint32_t Weight;
    uint8_t  RegisterNum;
    struct Node *Node_Next_Ptr;
} Node_Type;


typedef struct Port
{
    uint32_t	Weight;
    uint8_t  PortType;
    uint8_t  PortSn;
    uint16_t Node_Num;
    Node_Type *Node_Head_Ptr;
    uint32_t state;
    struct Port_Type *Port_Next_Ptr;
} Port_Type;

extern Node_Type *NodeHead_Ptr;
extern Node_Type *Node_Ptr;

extern Port_Type *PortHead_Ptr;
extern Port_Type *Port_Ptr;

//权重相关
#define SYS_CLK_BASE_NUM  1000
typedef struct
{
    uint32_t Clk_Base;
    uint32_t Clk_Cnt;
    uint32_t Port_USART_Cnt;
    uint32_t Temp_Clk;

} SYLife_Type;

extern SYLife_Type SY_LifeTime_loop; // 系统后台程序运行
extern SYLife_Type SY_LifeTime_dly;  // 延时函数





#define YES  1
#define NO   0


#define Ok                1
#define Ing               0
#define Err              -1

#define ON          			1
#define OFF         			0

#define NULL 							0

//void Delay_ms(uint32_t ms);
//void Delay_us(uint32_t us);
//uint32_t Sys_Broadcast(void);

//void Peripheral_Init(void);
//void Sys_Irq_conf(void);
//uint32_t Sys_VRF_Create(void);
//void Sys_VRF_Update(void);
uint32_t Find_Port(uint8_t  id);
uint32_t Find_Exist(uint8_t porttype,uint8_t * id);
//uint32_t Check_State(uint8_t porttype);



#endif

