#ifndef __BUZZER_H
#define __BUZZER_H
#include "common.h"

#define  proport          25000 //Tclk/(psc+1)=72000000/(7199+1)

//低 do re mi fa sol la si
#define  Do_L       ((proport/131)-1)
#define  Re_L       ((proport/147)-1)
#define  Mi_L       ((proport/165)-1)
#define  Fa_L       ((proport/176)-1)
#define  Sol_L      ((proport/196)-1)
#define  La_L       ((proport/220)-1)
#define  Si_L       ((proport/247)-1)
//中 do re mi fa sol la si
#define  Do_M       ((proport/262)-1)
#define  Re_M       ((proport/296)-1)
#define  Mi_M       ((proport/330)-1)
#define  Fa_M       ((proport/349)-1)
#define  Sol_M      ((proport/392)-1)
#define  La_M       ((proport/440)-1)
#define  Si_M       ((proport/494)-1)
//高 do re mi fa sol la si
#define  Do_H       ((proport/523)-1)
#define  Re_H       ((proport/587)-1)
#define  Mi_H       ((proport/659)-1)
#define  Fa_H       ((proport/699)-1)
#define  Sol_H      ((proport/784)-1)
#define  La_H       ((proport/880)-1)
#define  Si_H       ((proport/988)-1)



/******************************* 数据类型定义 ***************************/
typedef struct
{
  uint16_t mName; //元素1，音名，取值是上边的三种音名，低中高（1~7),0表是休止符
  uint16_t mTime; //元素2，时值，取值范围是全音符，半分音符等等
}Music_Score;

/********************************** 测试函数声明 ***************************************/
/****************************************************************************
作    者：lty
日    期：2019-12-03
函数功能：设置曲速
输入参数：曲速 单位:拍/分钟		  
输出参数：None
备    注：初始化设置
*****************************************************************************/
void AIQI_Music_Tempo(uint8_t t);

/****************************************************************************
作    者：lty
日    期：2019-12-03
函数功能：播放指定音乐
输入参数：music：乐谱
					volume：音量
输出参数：None
备    注：
*****************************************************************************/
void AIQI_Play_Music(Music_Score *music, uint8_t volume);

/****************************************************************************
作    者：lty
日    期：2019-12-03
函数功能：关闭蜂鸣器
输入参数：None  
输出参数：None
备    注：
*****************************************************************************/
void AIQI_Beep_Close(void);



#endif
