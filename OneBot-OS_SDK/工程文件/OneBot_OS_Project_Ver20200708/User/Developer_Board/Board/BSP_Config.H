#ifndef BSP_CONFIG_H
#define BSP_CONFIG_H
#include "type.h"

/*****************************************************************************/
//GPIO
#define OS_GPIO_ENABLE
//TIMER
#define OS_TIMER_ENABLE
//#define OS_SYSTICK_ENABLE
//#define OS_TIMER0_ENABLE
#define OS_TIMER1_ENABLE
//#define OS_TIMER2_ENABLE
#define OS_TIMER3_ENABLE
//#define OS_TIMER4_ENABLE
//#define OS_TIMER5_ENABLE
//#define OS_TIMER6_ENABLE
//#define OS_TIMER7_ENABLE
//#define OS_TIMER8_ENABLE
//#define OS_TIMER9_ENABLE
//#define OS_TIMER10_ENABLE
//#define OS_TIMER11_ENABLE
//#define OS_TIMER12_ENABLE
//#define OS_TIMER13_ENABLE
#define OS_TIMER14_ENABLE
#define OS_TIMER15_ENABLE
#define OS_TIMER16_ENABLE
#define OS_TIMER17_ENABLE
//#define OS_TIMER18_ENABLE
//#define OS_TIMER19_ENABLE
//#define OS_TIMER20_ENABLE
//PWM
#define OS_PWM_ENABLE
#define OS_PWM1_ENABLE
#define OS_PWM2_ENABLE
//#define OS_PWM3_ENABLE
//#define OS_PWM4_ENABLE
//#define OS_PWM5_ENABLE
//#define OS_PWM6_ENABLE
//#define OS_PWM7_ENABLE
//#define OS_PWM8_ENABLE
//#define OS_PWM9_ENABLE
//#define OS_PWM10_ENABLE
//#define OS_PWM11_ENABLE
//#define OS_PWM12_ENABLE
//#define OS_PWM13_ENABLE
//#define OS_PWM14_ENABLE
//#define OS_PWM15_ENABLE
//#define OS_PWM16_ENABLE
//#define OS_PWM17_ENABLE
//#define OS_PWM18_ENABLE
//#define OS_PWM19_ENABLE
//#define OS_PWM20_ENABLE
//IIC
#define OS_IIC_ENABLE
//#define OS_IIC0_ENABLE
#define OS_IIC1_ENABLE
//#define OS_IIC2_ENABLE
//#define OS_IIC3_ENABLE
//#define OS_IIC4_ENABLE
//#define OS_IIC5_ENABLE
//#define OS_IIC6_ENABLE
//SPI
//#define OS_SPI_ENABLE
//#define OS_SPI0_ENABLE
//#define OS_SPI1_ENABLE
//#define OS_SPI2_ENABLE
//#define OS_SPI3_ENABLE
//#define OS_SPI4_ENABLE
//#define OS_SPI5_ENABLE
//#define OS_SPI6_ENABLE
//UART
#define OS_UART_ENABLE
//#define OS_UART0_ENABLE
#define OS_UART1_ENABLE
#define OS_UART2_ENABLE
//#define OS_UART3_ENABLE
//#define OS_UART4_ENABLE
//#define OS_UART5_ENABLE
//#define OS_UART6_ENABLE
//#define OS_UART7_ENABLE
//#define OS_UART8_ENABLE
//#define OS_UART9_ENABLE
//#define OS_UART10_ENABLE
#define OS_PRINTF_ENABLE  //wpbadd 2019-02-21

//INTERNAL FLASH
#define OS_INTERNAL_FLASH_ENABLE
//ADC
#define OS_ADC_ENABLE
//#define OS_ADC0_ENABLE
#define OS_ADC1_ENABLE
//#define OS_ADC2_ENABLE
//#define OS_ADC3_ENABLE
//#define OS_ADC4_ENABLE
/*****************************************************************************/
#endif
