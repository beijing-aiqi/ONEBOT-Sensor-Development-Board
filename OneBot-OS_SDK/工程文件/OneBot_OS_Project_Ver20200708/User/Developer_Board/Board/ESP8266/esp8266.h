#ifndef  __AIQI_WIFI_H
#define	 __AIQI_WIFI_H
#include "common.h"

#if defined ( __CC_ARM   )
#pragma anon_unions
#endif

/******************************* 数据结构体定义 ***************************/
typedef enum{
  STA,
  AP,
  STA_AP  
} ENUM_Net_ModeTypeDef;

typedef enum{
	 enumTCP,
	 enumUDP,
} ENUM_NetPro_TypeDef;
	
typedef enum{
	Multiple_ID_0 = 0,
	Multiple_ID_1 = 1,
	Multiple_ID_2 = 2,
	Multiple_ID_3 = 3,
	Multiple_ID_4 = 4,
	Single_ID_0 = 5,
} ENUM_ID_NO_TypeDef;
	
typedef enum{
	OPEN = 0,
	WEP = 1,
	WPA_PSK = 2,
	WPA2_PSK = 3,
	WPA_WPA2_PSK = 4,
} ENUM_AP_PsdMode_TypeDef;

typedef enum{
	WIFI_SSID_LEN = 40,
	WIFI_PWD_LEN  = 70,
	WIFI_TCPSRV_IP_LEN = 20,
	WIFI_TCPSRV_COM_LEN = 10,
}WIFI_DATA_LEN;

typedef struct WiFi_Infor
{
	char SSID_Router[WIFI_SSID_LEN];
	char PassWord_Router[WIFI_PWD_LEN];
	char IPAddr_tcpServer[WIFI_TCPSRV_IP_LEN];
	char ComNum_tcpServer[WIFI_TCPSRV_COM_LEN];
}WiFi_Infor_Inst;



/******************************* 外部全局变量声明 ***************************/
#define     AIQI_WIFI_RST_HIGH_LEVEL()   Write("ESP_RST",OS_NULL,OS_GPIO_HIGH)
#define     AIQI_WIFI_RST_LOW_LEVEL()    Write("ESP_RST",OS_NULL,OS_GPIO_LOW)         
extern volatile uint8_t ucTcpClosedFlag;

/********************************** 用户需要设置的参数**********************************/
#define   AIQI_WIFI_BulitApSsid         "OneBotOS"      	//要建立的热点的名称
#define   AIQI_WIFI_BulitApEcn           OPEN             //要建立的热点的加密方式
#define   AIQI_WIFI_BulitApPwd           "123456"         //要建立的热点的密钥
#define   AIQI_WIFI_TcpServer_IP         "192.168.123.1"  //服务器开启的IP地址
#define   AIQI_WIFI_TcpServer_Port       "8080"           //服务器开启的端口   
#define   AIQI_WIFI_TcpServer_OverTime   "1800"           //服务器超时时间（单位：秒）

/********************************** 测试函数声明 ***************************************/
/****************************************************************************
作    者：Wu Pengbo
日    期：2019-04-05
函数功能：AIQI_WIFI （Sta Tcp Client）透传 连接
输入参数：pSSID_Router     : 路由器SSID，不超过32个字节
					pPassWord_Router : 路由器密码，不超过63个字节
					tcpServer_IpAddr : 局域网内作为TcpServer的PC的 IP地址，不超过15字节
					tcpServer_ComNum : 局域网内作为TcpServer的PC的 端口号，不超过4字节
输出参数：1: 连接成功
					0: 连接失败	
备    注：
*****************************************************************************/
uint8_t AIQI_WIFI_StaTcpClient_Connect( 	char *pSSID_Router, char *pPassWord_Router,
									char *tcpServer_IpAddr, char *tcpServer_ComNum);
												
/****************************************************************************
作    者：Wu Pengbo
日    期：2019-04-05
函数功能：AIQI_WIFI （Sta Tcp Client）透传 发送字符串
输入参数：pStr: 要发送的字符串
输出参数：1，发送成功
          0，发送失败
备    注：
*****************************************************************************/
uint8_t AIQI_WIFI_StaTcpClient_SendString( char *pStr);

/****************************************************************************
作    者：Wu Pengbo
日    期：2019-04-05
函数功能：AIQI_WIFI （Sta Tcp Client）透传 接收字符串
输入参数：RxMode: 
			  1-阻塞式等待数据; 没有收到数据，会一直等待
			  0-查询式
输出参数：pStr: 接收到的字符串
输出参数：0  : 无数据
		  非0: 接收数据长度
备    注：
*****************************************************************************/
uint8_t AIQI_WIFI_StaTcpClient_RecvString(char *pStr, uint8_t RxMode );

/****************************************************************************
作    者：Wu Pengbo
日    期：2019-04-5
函数功能：AIQI_WIFI模块接收字符串
输入参数：enumEnUnvarnishTx，声明是否已使能了透传模式
						ENABLE-透传模式
						DISABLE-非透传模式
					pRecStr，接收的字符串首地址
          RxMode: 
          	1-阻塞式等待数据; 没有收到数据，会一直等待
          	0-查询式
输出参数：0  : 无数据
		  非0: 接收数据长度
备    注：
*****************************************************************************/
uint8_t AIQI_WIFI_ReceiveString ( FunctionalState enumEnUnvarnishTx,char *pRecStr,uint8_t RxMode );

/****************************************************************************
作    者：LTY
日    期：2019-10-19
函数功能：AIQI_WIFI模块TCP连接外部服务器
输入参数：mode 服务器连接协议,TCP UDP
					host，服务器IP字符串
					ComNum，服务器端口字符串
					delay，允许超时
输出参数：1，连接成功
          0，连接失败
备    注：
*****************************************************************************/
uint8_t AIQI_WIFI_Link_COMMServer ( char *mode,  char *host,  char *ComNum, int delay);

/****************************************************************************
作    者：LTY
日    期：2019-10-23
函数功能：向指定服务器发送数据
输入参数：LinkMode，服务器连接协议,TCP UDP
					host，服务器域名或IP字符串
					ComNum，服务器端口字符串 
					SendMode， get或post
					url，路径及要发送的数据
					RxBuf，接收到的数据缓存区
					delay，允许超时，包括连接服务器超时+发送超时+接收超时
输出参数：1，发送成功
					0，发送失败
					3，服务器连接失败
					4，未收到回复
					RxBuf接收到的数据
备    注：
*****************************************************************************/
uint8_t AIQI_WIFI_Send_COMMServer (char *LinkMode,char *host,  char *ComNum, char *SendMode, char *url,char *RxBuf, int delay);

/****************************************************************************
作    者：lty
日    期：2019-10-19
函数功能：查询设备ip
输入参数：ip及MAC缓存区	  
输出参数：设备ip，MAC
备    注：
*****************************************************************************/
uint8_t AIQI_WIFI_MyIp (char *myIP,char *myMAC);

/****************************************************************************
作    者：LTY
日    期：2019-11-1
函数功能：向OneBotIOT云平台发送数据
输入参数：username：用户名
					userpwd：用户密码
					name：设备名
					state：数据
输出参数：1，发送成功
					0，发送失败
备    注：
*****************************************************************************/
uint8_t OneBotYunSet( char *username, char *userpwd, char *name, int state);

/****************************************************************************
作    者：LTY
日    期：2019-11-4
函数功能：向OneBotIOT云平台接收数据
输入参数：username：用户名
					userpwd：用户密码
					name：设备名
输出参数：接收到的数据
备    注：注意设备名不可为中文，数据只能为int
*****************************************************************************/
int OneBotYunGet(char *username,char *userpwd,char *name);

/****************************************** AIQI_WIFI 函数声明 ***********************************************/
void                     		AIQI_WIFI_Init                        ( void );
void                     		AIQI_WIFI_Rst                         ( void );
uint8_t 								 		AIQI_WIFI_Cmd ( char *cmd, char *reply1, char *reply2, char *erreply1, char *erreply2, uint32_t waittime );
void                     		AIQI_WIFI_AT_Test                     ( void );
uint8_t                     AIQI_WIFI_Net_Mode_Choose             ( ENUM_Net_ModeTypeDef enumMode );
uint8_t                     AIQI_WIFI_JoinAP                      ( char *pSSID, char *pPassWord,int delay );
uint8_t                     AIQI_WIFI_BuildAP                     ( char * pSSID, char * pPassWord, ENUM_AP_PsdMode_TypeDef enunPsdMode );
uint8_t                     AIQI_WIFI_Enable_MultipleId           ( FunctionalState enumEnUnvarnishTx );
uint8_t                     AIQI_WIFI_Link_Server                 ( ENUM_NetPro_TypeDef enumE, char * ip, char * ComNum, ENUM_ID_NO_TypeDef id);
uint8_t                     AIQI_WIFI_StartOrShutServer           ( FunctionalState enumMode, char * pPortNum, char * pTimeOver );
uint8_t                 		AIQI_WIFI_Get_LinkStatus              ( void );
uint8_t                 		AIQI_WIFI_Get_IdLinkStatus            ( void );
uint8_t                  		AIQI_WIFI_Inquire_ApIp                ( char * pApIp, uint8_t ucArrayLength );
uint8_t                     AIQI_WIFI_UnvarnishSend               ( void );
void                     		AIQI_WIFI_ExitUnvarnishSend           ( void );
uint8_t                     AIQI_WIFI_SendString                  ( FunctionalState enumEnUnvarnishTx, char * pStr, uint32_t ulStrLength, ENUM_ID_NO_TypeDef ucId );
uint8_t                  		AIQI_WIFI_CWLIF                       ( char * pStaIp );
uint8_t                  		AIQI_WIFI_CIPAP                       ( char * pApIp );
#endif
