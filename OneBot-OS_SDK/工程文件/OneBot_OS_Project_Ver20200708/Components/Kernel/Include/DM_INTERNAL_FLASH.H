/*****************************************************************************************************************************************************
 文件：DM_INTERANL_FLASH.H
 版本：V1.0.0
 介绍：INTERNAL FLASH设备模型的类型定义和函数声明
*****************************************************************************************************************************************************/
#ifndef __DM_INTERNALFLASH_H
#define __DM_INTERNALFLASH_H
/*Include-------------------------------------------------------------------------------------------------------------------------------------------*/
#include "type.h"
#include "Device.H"
/*Type----------------------------------------------------------------------------------------------------------------------------------------------*/
typedef struct flash_configure
{
  uint32_t                                        baseaddr;
  uint32_t                                        pagesize;
  uint8_t                                         pagecount;
} os_flash_configure_t;

typedef struct  flash_device
{
	struct device                                   parent;
  struct flash_configure                          configure;
	const struct os_flash_ops                       *ops;
  uint32_t                                        write_offsetaddr;
  uint32_t                                        read_offsetaddr;
} os_flash_device_t;

struct os_flash_ops
{
  os_status_t                                    (*open)(os_flash_device_t *flash,uint16_t flag);
  os_status_t                                    (*close)(os_flash_device_t *flash);
  
  uint32_t                                       (*read)(os_flash_device_t *flash ,uint16_t flag,uint16_t * pBuffer, uint16_t size );	//wpbchange 20190305
  uint32_t                                       (*write)(os_flash_device_t *flash,uint16_t flag,uint16_t * pBuffer, uint16_t size);		//wpbchange 20190305
//  uint32_t                                       (*read)(os_flash_device_t *flash,uint16_t flag, uint32_t *world, uint16_t size );
//  uint32_t                                       (*write)(os_flash_device_t *flash,uint16_t flag,uint32_t word, uint16_t size);
};


/*****************************************************************************************************************************************************
 �������ڣ�    κ����
 �������ܣ�    flash�豸ע��
 �汾    ��    V1.0
 ����    ��    flash        �豸
               name         �豸����
               flag         ��־
               data         ˽������
 ����ֵ  ��    ���óɹ����� OS_SUCCESS
 ��ע    ��    None
*****************************************************************************************************************************************************/
extern os_status_t flash_register(os_flash_device_t *flash, const char *name,uint32_t flag, void *data);

#endif


