/*****************************************************************************************************************************************************
 文件：DM_IIC.H
 版本：V1.0.0
 介绍：IIC设备模型的类型定义和函数声明
*****************************************************************************************************************************************************/
#ifndef __DM_IIC_H
#define __DM_IIC_H
/*Include-------------------------------------------------------------------------------------------------------------------------------------------*/
#include "type.h"
#include "Device.H"
/*Marco---------------------------------------------------------------------------------------------------------------------------------------------*/

/*Type----------------------------------------------------------------------------------------------------------------------------------------------*/
typedef struct iic_configure
{
  uint8_t  direction;
	uint32_t mode;      //100 400 
  uint8_t  addrformat;//7bit 10bit
  uint16_t  salveaddr;
  uint16_t selfaddr;
  uint8_t Priority;
} os_iic_configure_t;

typedef struct iic_pin
{
	uint8_t ClkPin;
	uint8_t DataPin;
} os_iic_pin_t;


typedef struct iic_device
{
	struct device                                   parent;
	const struct os_iic_ops                        *ops;
	struct iic_configure                            configure;
    os_iic_pin_t                                    iic_pin;
} os_iic_device_t;

typedef struct iicbus_device
{
	struct device                                   parent;
    os_iic_device_t                                 iic_device;
} os_iicbus_device_t;

struct os_iic_ops
{
  os_status_t                                     (*configure)(os_iic_device_t *iic,os_iic_configure_t *arg);
  os_status_t                                     (*open)(os_iic_device_t *iic,uint16_t flag);
  os_status_t                                     (*close)(os_iic_device_t *iic);
  uint32_t                                        (*read)(os_iic_device_t *iic ,uint8_t *data,os_size_t size);
  uint32_t                                        (*write)(os_iic_device_t *iic,uint8_t *data,os_size_t size);
};
/*****************************************************************************************************************************************************
 作者日期：    魏巧雷 
 函数功能：    IIC设备注册
 版本    ：    V1.0
 输入    ：    iic         设备
               name         名字
 返回值  ：    成功返回     OS_SUCCESS
 备注    ：    None
*****************************************************************************************************************************************************/
extern os_status_t iic_register(os_iic_device_t *iic, const char *name,uint32_t flag, void *data);




#endif

