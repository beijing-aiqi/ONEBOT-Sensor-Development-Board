/*****************************************************************************************************************************************************
 文件：DM_SPI.H
 版本：V1.0.0
 时间：2017-12-01
 介绍：SPI设备模型的类型定义和函数声明
*****************************************************************************************************************************************************/
#ifndef __DM_SPI_H
#define __DM_SPI_H
/*Include-------------------------------------------------------------------------------------------------------------------------------------------*/
#include "type.h"
#include "Device.H"
/*Marco---------------------------------------------------------------------------------------------------------------------------------------------*/

#define SPI_CONFIG_DEFAULT                    \
{                                             \
  OS_SPI_CLK_LOW,                             \
  OS_SPI_MASTER,                              \
  OS_SPI_MSB,                                 \
  OS_SPI_MODE_0,                              \
  OS_SPI_4WIRE,                               \
  OS_SPI_HAVE_CS,                             \
  OS_SPI_CS_LOW,                              \
}
/*Type----------------------------------------------------------------------------------------------------------------------------------------------*/
typedef struct spi_configure
{
	uint32_t max_clock_hz;
	uint8_t  direction;
  uint8_t  fistbit;
	uint8_t  mode;
	uint8_t  wire;
	uint8_t  have_cs;
	uint8_t  cs_active;
  uint8_t  Priority;
} os_spi_configure_t;

typedef struct spi_pin
{
	uint8_t CsPin;
	uint8_t ClkPin;
	uint8_t MisoPin;
	uint8_t MosiPin;
} os_spi_pin_t;


typedef struct spi_device
{
	struct device                                   parent;
	const struct os_spi_ops                        *ops;
	struct spi_configure                            configure;
  os_spi_pin_t                                    spi_pin;
} os_spi_device_t;

typedef struct spibus_device
{
	struct device                                   parent;
  os_spi_device_t                                 spi_device;
} os_spibus_device_t;

struct os_spi_ops
{
	os_status_t                                    (*configure)(os_spi_device_t *spi,os_spi_configure_t *cfg);
    os_status_t                                   (*open)(os_spi_device_t *spi,uint16_t flag);
  os_status_t                                     (*close)(os_spi_device_t *spi);
  uint8_t                                        (*rwbyte)(os_spi_device_t *spi,uint8_t data);
  os_status_t                                    (*cs_pin_control)(os_spi_device_t *spi,uint8_t state);
  
};
/*****************************************************************************************************************************************************
 作者日期：    魏巧雷
 函数功能：    SPI设备注册
 版本    ：    V1.0
 输入    ：    spi         设备
               name         名字
 返回值  ：    成功返回     OS_SUCCESS
 备注    ：    None
*****************************************************************************************************************************************************/
extern os_status_t spi_register(os_spi_device_t *spi, const char *name,uint32_t flag, void *data);
#endif
