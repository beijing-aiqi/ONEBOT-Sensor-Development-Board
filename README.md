# ONEBOT Sensor Development Board

#### 介绍
小米生态链爱其科技推出的开源传感器创作平台，提供了机器人、物联网应用场景创作原型，让每个人都享受创造科技的乐趣！

#### 使用说明

1.  三明治结构 积木式拼装
2.  把组装好的传感器通过USB转TPYE-C线插入PC机，下载程序即可使用。
3.  安装到主控 开启十八种传感器功能，增强主控玩法。

注意：
1：具体使用方式及注意事项 参见用户手册。
2：适配的主控包括 米兔积木机器人、小米智能积木、爱其ONEBOT机器人

官方购买网址：
https://item.taobao.com/item.htm?spm=a2oq0.12575281.0.0.8a841debXhImJk&ft=t&id=617133577653

#### 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 码云特技
1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
